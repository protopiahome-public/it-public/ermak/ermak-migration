<?php
	/*
Plugin Name: Ermak.Migration
Plugin URI: http://wp-ermak.ru/?page_id=135&d_d=130
Description: Converter for all Metagame data to json and vice versa.
Version: 1.0.1
Date: 28.08.2015
Author: Genagl
Author URI: http://www.genagl.ru
License: GPL2
 Copyright 2015  Genagl  (email: genag1@list.ru)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/ 

//���������� ���������
function init_textdomain_ermak_migration() 
{ 
	if (function_exists('load_plugin_textdomain')) 
	{
		load_plugin_textdomain("ermak_migration", false , dirname( plugin_basename( __FILE__ ) ) .'/lang/');     
	} 
}
// �������� �� �� ������ ���������� ������� 
add_action('plugins_loaded', 'init_textdomain_ermak_migration');



function emig_control_plugins()
{
	global $eb_sticker;
	$arr		= array();
	if(!is_plugin_active('Ermak/Ermak.php'))
	{
		$arr[]	= __('Ermak. Metagame', "ermak_mogration");
	}
	if(count($arr)==0)	return true;
	$eb_sticker		= '<div><board_title>'.__("critical gaps plugin functionality Ermak Migration", "ermak_mogration")."</board_title>";
	$eb_sticker		.= __('Please install the following plugins', "ermak_mogration").'<ul>';
	foreach($arr as $a)
	{
		$eb_sticker		.= "<li>".$a."</li>";
	}
	$eb_sticker		.= "</ul>".__("Sorry. Ermak Migration not working", "ermak_mogration")."</div>";
	add_action( 'admin_notices',					'after_install_sticker_emig' , 23);
	return false;
}
function after_install_sticker_emig()
{
	global $eb_sticker;
	echo "
	<div class='updated notice is-dismissible1' id='install_eb_notice' style='padding:30px!important; position: relative;'>
		".
			$eb_sticker .
		"
		<span class='smc_desmiss_button'>
			<span class='screen-reader-text'>".__("Close")."</span>
		</span>
	</div>
	
	";
}
if(!emig_control_plugins())	return;

//Paths
define('ERMAK_MIGRATION_URLPATH', WP_PLUGIN_URL.'/Ermak_Migration/');
define('ERMAK_MIGRATION_REAL_PATH', WP_PLUGIN_DIR.'/'.plugin_basename(dirname(__FILE__)).'/');


		add_action( 'admin_menu', 'admin_page_handler', 24);
		function admin_page_handler()
		{
			add_menu_page( 
						__("Ermak. Migration", "ermak_migration"), // title
						__("Ermak. Migration", "ermak_migration"), // name in menu
						'manage_options', // capabilities
						'Ermak_Migration_page', // slug
						'Migration_setting_pages', // options function name
						'dashicons-migrate', 
						'20.5678'
						);
		}		

		function Migration_setting_pages()
		{
			require_once(ERMAK_MIGRATION_REAL_PATH."tpl/settings.php");
		}
		function migrat()
		{
			require_once(ERMAK_MIGRATION_REAL_PATH."tpl/migrat.php");
		}
		function get_migr_result($arr, $name)
		{
			require_once(ERMAK_MIGRATION_REAL_PATH."tpl/get_migr_result.php");	
			return $result_html;
		}
		function ermak_migration_install_scenario($name)
		{
			ob_start();
			require_once(ERMAK_MIGRATION_REAL_PATH."tpl/install_scenario.php");
			$var 	= ob_get_contents();
			ob_end_clean();
			return $httml;
		}
		function ermak_migration_download_excel($name)
		{
			$extension_loaded 		= "";
			if(!extension_loaded('gd2'))
				$extension_loaded 	.=  "<p>" . sprintf(__("Extension %s for PHP is no installed", "ermak_migration"), "<b>php_gd2</b>") . "</p>";
			if(!extension_loaded('zip'))
				$extension_loaded 	.=  "<p>" . sprintf(__("Extension %s for PHP is no installed", "ermak_migration"), "<b>php_zip</b>") . "</p>";
			if(!extension_loaded('xml'))
				$extension_loaded 	.=  "<p>" . sprintf(__("Extension %s for PHP is no installed", "ermak_migration"), "<b>php_xml</b>") . "</p>";
			if($extension_loaded != "")
				return array(false, $extension_loaded);
			ob_start();
			require_once(ERMAK_MIGRATION_REAL_PATH."tpl/export_excel.php");
			$httml 	= ob_get_contents();
			ob_end_clean();
			return array(true, $httml);
		}
		function ermak_clear_all()
		{
			require_once(ERMAK_MIGRATION_REAL_PATH."tpl/clear_all.php");
		}
		
		add_filter("smc_get_object", "smc_get_object", 11, 2);
		function smc_get_object( $object)
		{
			global $new_dir;
			if(!is_array($object)) return $object;
			foreach($object as $key=>$val)
			{
				if(( $key=="_thumbnail_id" || $key=="png_url" ) && !($val=="." || $val==""))
				{
					$stt				= (strrpos($val, '/'))+1;
					$fnn				= (strrpos($val, '.')) - $stt;
					$filename 			= substr($val,  $stt, $fnn);
					$thumbnail 			= substr($val,  $stt);
					$wp_check_filetype 	= wp_check_filetype($val);
					@file_copy($val, $new_dir ."/".  $filename . "." . $wp_check_filetype['ext']);
					if($filename)
						$object[$key]	= $filename . "." . $wp_check_filetype['ext'];
					else
						unset($object[$key]);
				}
			}
			return $object;
		}
		
		add_action( 'admin_enqueue_scripts', 'admin_er_mig_scripts_add' );
		function admin_er_mig_scripts_add()
		{
			//css
			wp_register_style('er_mig_admin-css', ERMAK_MIGRATION_URLPATH . 'css/er_mig_admin.css', array());
			wp_enqueue_style('er_mig_admin-css');
			//js
			wp_register_script('er_mig_admin', ERMAK_MIGRATION_URLPATH . 'js/er_mig_admin.js', array());
			wp_enqueue_script('er_mig_admin');		
		}
		
		add_action('smc_myajax_admin_submit', 'ermk_migration_myajax_submit', 70);
		function ermk_migration_myajax_submit($params)
		{
			global $start;
			$html			= 'none!';
			switch($params[0])
			{
				case 'download_scenario':
					$arr				= $params[1];
					$name				= $params[2];
					$dar				= get_migr_result($arr, $name);
					$d					= array(	
													$params[0], 
													array(
															'text'	=> $dar,
															'time'	=> ( getmicrotime()  - $start )																
														  )
												);
					$d_obj				= json_encode($d);
					print $d_obj;
					break;
				case 'install_scenario':
					$evt				= ermak_migration_install_scenario($params[1]);
					$d					= array(	
													$params[0], 
													array(
															'text'	=> "<h2>".__("Successfull","ermak_migration")."</h2>".$evt,
															'time'	=> ( getmicrotime()  - $start )																
														  )
												);
					$d_obj				= json_encode($d);
					print $d_obj;
					break;
				case 'download_excel':
					list($successfull, $data) 	= ermak_migration_download_excel($params[1]);
					$d					= array(	
													$params[0], 
													array(
															'text'	=> $successfull ? "<h2>".__("Successfull", "ermak_migration")."</h2>".$data : "<h2>".__("Not successfull", "ermak_migration")."</h2>".$data,
															'time'	=> ( getmicrotime()  - $start )																
														  )
												);
					$d_obj				= json_encode($d);
					print $d_obj;
					break;
			}
		}
		add_filter("ermak_migration_correct_option", "ermak_migration_correct_option", 10, 2);
		function ermak_migration_correct_option($option_data, $option_name)
		{
			switch($option_name)
			{
				case "smc_map_data":
					$foo	= explode("!!!!", $option_data);
					$str	= "";
					$i =0;
					foreach($foo as $elem)
					{
						if(++$i % 2 !=0)	
						{
							$str .= $elem;
							continue;
						}
						$location	= get_term_by("slug", $elem, SMC_LOCATION_NAME);
						$str .= $location->term_id;
					}
					$option_data = $str;
					break;
				
				default:				
					break;
			}
			return $option_data;
		}
?>