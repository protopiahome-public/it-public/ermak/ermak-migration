<?php
	global $httml, $migration_url, $components;
	require_once(SMC_REAL_PATH."class/SMC_Object_type.php");
	$SMC_Object_type	= SMC_Object_type::get_instance();
	//ermak_clear_all();
	
	//return ERMAK_MIGRATION_REAL_PATH."temp/".$name."/data.json";
	//return "";
	require_once(ERMAK_MIGRATION_REAL_PATH."tpl/clear_all.php");
	define('ERMAK_MIGRATION_PATH', ERMAK_MIGRATION_REAL_PATH."temp/".$name."/");
	$migration_url = ERMAK_MIGRATION_URLPATH."temp/".$name."/";
	$contents 	= file_get_contents(ERMAK_MIGRATION_REAL_PATH."temp/".$name."/data.json");
	$httml .=  "<p>".$migration_url."</p>";
	/*
	$httml .=  "<h2>BEF0RE: ".mb_detect_encoding($contents)."</h2>";
	//$contents = mb_convert_encoding( $contents, 'UTF-8' );
	//$contents = iconv('cp1251', 'UTF-8', $contents);
	$contents = iconv('ASCII', 'UTF-8', $contents);
	$contents = utf8_encode($contents);
	$httml .=  "<h2>AFTER: ".mb_detect_encoding($contents)."</h2>";
	//return;
	
	*/
	
	//$httml .=  "<DIV style='size:20px;'>".Assistants::$httml .= _me($SMC_Object_type->object)."</div>";
	$data		= json_decode($contents, true);
	//$arr2		= $data["smc_map_data"]['data'];
	//$httml .=  Assistants::$httml .= _me($arr2, true);
	//$httml .=  Assistants::$httml .= _me($data, true);
	//return;
	
	$ret		= array();
	foreach($data as $dat)
	{
		if(isset($ret["post_type"]))
			$ret[ $dat["post_type"]]++;
		else
			$ret["post_type"] = 0;
		$d					=  $SMC_Object_type->get($dat['post_type']);
		$httml .=  '<h3>'. $dat['post_type']."</h3>";
		switch( $dat['obj_type'] )
		{		
			case "post":
				
				
				$httml .=  "<p>post_title: ". $dat['title'] ."</p>";
				
				$post_data	= array(
									  'post_title'   	=> $dat['title'],
									  'post_name'    	=> $dat['name'],
									  'post_content' 	=> $dat['text'],
									  'post_type' 		=> $dat['post_type'],
									  'post_status'		=> 'publish'
								   );
				$post_id 	= wp_insert_post( $post_data );			
				$SMC_Object_type->insert_post_meta($dat, $post_id , $dat['post_type']);				
				break;
			case "taxonomy":
				$httml .=  '<h3>'. $dat['post_type']."</h3>";
				$id		= wp_insert_term( $dat['title'], $dat['post_type'], array('slug'=>$dat['name']));
				$term_meta	= array();
				foreach($dat as $key=>$val)
				{							
					$foo		= $SMC_Object_type->convert_id($key, $val, $d, $id);
					if( is_wp_error($foo) )	
					{
						$httml .=   $foo->get_error_message();
					}
					else
					{
						$term_meta[$key]	= $foo;
					}
				}
				switch($dat['post_type'])
				{
					case SMC_LOCATION_NAME:						
						SMC_Location::update_taxonomy_custom_meta( $id['term_id'], $term_meta, true );
						break;
					case "smp_routh":
						SMP_Routh::update_term_meta($id['term_id'], $term_meta);
						break;
				}
				break;
			case "option":
				$httml .=  "<div>" . $dat['merge_type'] . "</div>";
				$volume	= apply_filters("ermak_migration_correct_option", $dat['data'], $dat['post_type']);
				switch( $dat['merge_type'])
				{
					case MERGE_OPTION:
						$option		= get_option($dat['post_type']);
						foreach($option as $key=>$val)
						{
							if(isset($volume[$key]))
							{	
								continue;								
							}
							else	
							{
								$volume[$key]	= iconv('ASCII', 'UTF-8', $val);		
							}			
						}
						update_option($dat['post_type'], $volume);
						break;
					case CHANGE_OPTION:
					default:
						update_option($dat['post_type'], $volume);
						break;
				}
				break;
		}
	}
	foreach($data as $dat)
	{
		switch( $dat['obj_type'] )
		{		
			case "taxonomy":
				$term	= get_term_by( "slug", $dat['name'],	$dat['post_type'] );
				$parent	= get_term_by( "slug", $dat['parent'],	$dat['post_type'] );
				$httml .=  '<p>' .( $dat['parent'] ). ", term_id = ". $term->term_id . '</p>';
				wp_update_term($term->term_id,  $dat['post_type'], array( "parent"=>$parent->term_id ));
		}
	}
	foreach($components as $key=>$val)
	{
		update_post_meta( $val['id'], $val['key'], $SMC_Object_type->convert_array($val['value']) );
	}
	//update_option(SMC_ID, 			$data[SMC_ID]['data']);
	//update_option("smc_map_data", 	$data["smc_map_data"]['data']);
	return $httml;
