<?php
/**/
			
			require_once(SMC_REAL_PATH."class/SMC_Object_type.php");
			$SMC_Object_type	= SMC_Object_type::get_instance();
			
			//echo Assistants::echo_me($SMC_Object_type->object_property_args("SMC_Location", "get_instance", 4));
			//echo Assistants::echo_me($SMC_Object_type->object);
			
			$keys				= array_keys($SMC_Object_type->object);
			$values				= array_values($SMC_Object_type->object);
			//echo Assistants::echo_me($keys);
			//return;
			$all				= array();
			for($i=0; $i<count($keys); $i++)
			{
				switch( $SMC_Object_type->get_type($keys[$i]))
				{
					case "post":
					case "page":
						$args		= array(
											"numberposts"		=> -1,
											"offset"			=> 0,
											'orderby'  			=> 'title',
											'order'     		=> 'ASC',
											'fields'			=> 'ids',
											'post_type' 		=> $keys[$i],
											'post_status' 		=> 'publish',									
										);
					
						$posts		= get_posts($args);
						$html		.= "<h3>".$keys[$i]."</h3>";
						foreach($posts as $post)
						{
							$elem	= $SMC_Object_type->get_object($post, $keys[$i]);
							$html	.= "<div class=abzaz>" . json_encode($elem, JSON_UNESCAPED_UNICODE ). "</div>";
							$all[]	= $elem;
							//$html	.= "<div class=abzaz>" . Assistants::echo_me($post ). "</div>";
						}
						break;
					case "taxonomy":
						
						$args		= array(
											'number' 		=> 120
											,'offset' 		=> 0
											,'orderby' 		=> 'id'
											,'order' 		=> 'ASC'
											,'hide_empty' 	=> false
											,'fields' 		=> 'all'
											,'slug' 		=> ''
											,'hierarchical' => true
											,'name__like' 	=> ''
											,'pad_counts' 	=> false
											,'get' 			=> ''
										);
								
						$childLocs			= get_terms($keys[$i], $args);
						
						$html		.= "<h3>".$keys[$i]."</h3>";
						foreach($childLocs as $post)
						{
							$elem	= $SMC_Object_type->get_object($post->term_id, $keys[$i]);
							$html	.= "<div class=abzaz>" . json_encode($elem, JSON_UNESCAPED_UNICODE). "</div>";
							$all[]	= $elem;
						}
						break;
					case "option":
						$html		.= "<h4>option: ".$keys[$i]."</h4>";
						$html		.= "<div class=abzaz>" .   Assistants::echo_me($values[$i]['data'], true). "</div>";
						$all[]		= $values[$i];
						break;
					default:
						$html		.= "<h4>".$keys[$i]."</h4>";
						$html		.= "<div class=abzaz>" . $values[$i]. "</div>";
						//$all[]		= $values[$i];
				}
			}
			//echo json_encode($all, JSON_UNESCAPED_UNICODE);
			echo $html;
			
?>