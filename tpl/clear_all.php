<?php
	$SMC_Object_type	= SMC_Object_type::get_instance();
	$keys				= array_keys($SMC_Object_type->object);
	$values				= array_values($SMC_Object_type->object);
	for($i=0; $i<count($keys); $i++)
			{
				
				switch( $SMC_Object_type->get_type($keys[$i]))
				{
					case "post":
					case "page":
						$args		= array(
											"numberposts"		=> -1,
											"offset"			=> 0,
											'orderby'  			=> 'title',
											'order'     		=> 'ASC',
											'fields'			=> 'ids',
											'post_type' 		=> $keys[$i],
											'post_status' 		=> 'publish',									
										);
					
						$posts		= get_posts($args);		
						//echo $keys[$i].'-'.count($posts);
						foreach($posts as $post)
						{
							//Assistants::echo_me($post);
							wp_delete_post($post, true);
						}
						break;
					case "taxonomy":
						$args		= array(
											'number' 		=> 120
											,'offset' 		=> 0
											,'orderby' 		=> 'id'
											,'order' 		=> 'ASC'
											,'hide_empty' 	=> false
											,'fields' 		=> 'all'
											,'slug' 		=> ''
											,'hierarchical' => true
											,'name__like' 	=> ''
											,'pad_counts' 	=> false
											,'get' 			=> ''
										);
								
						$childLocs			= get_terms($keys[$i], $args);						
						foreach($childLocs as $post)
						{
							wp_delete_term( $post->term_id, $keys[$i] );
						}
						break;
					case "option":
						//delete_option($keys[$i] );
						break;
					default:
						null;
				}
			}
?>